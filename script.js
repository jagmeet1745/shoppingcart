var data = [{
    "name": "mango",
    "category": "fruit",
    "price": "80"
}, {
    "name": "orange",
    "category": "fruit",
    "price": "80"
}, {
    "name": "kiwi",
    "category": "fruit",
    "price": "20"
}, {
    "name": "banana",
    "category": "fruit",
    "price": "40"
}, {
    "name": "pomegranate",
    "category": "fruit",
    "price": "100"
}, {
    "name": "blackcurrant",
    "category": "fruit",
    "price": "1000"
}, {
    "name": "avocado",
    "category": "fruit",
    "price": "1000"
}, {
    "name": "grapes",
    "category": "fruit",
    "price": "50"
}, {
    "name": "strawberry",
    "category": "fruit",
    "price": "40"
}, {
    "name": "peach",
    "category": "fruit",
    "price": "60"
}, {
    "name": "broccoli",
    "category": "vegetable",
    "price": "300"
}, {
    "name": "cabbage",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "potato",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "carrot",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "tomato",
    "category": "vegetable",
    "price": "100"
}, {
    "name": "turnip",
    "category": "vegetable",
    "price": "150"
}, {
    "name": "pumpkin",
    "category": "vegetable",
    "price": "300"
}, {
    "name": "onion",
    "category": "vegetable",
    "price": "200"
}, {
    "name": "mushroom",
    "category": "vegetable",
    "price": "500"
}, {
    "name": "capsicum",
    "category": "vegetable",
    "price": "200"
}, {
    "name": "oreo",
    "category": "biscuit",
    "price": "35"
}, {
    "name": "goodday",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "parleg",
    "category": "biscuit",
    "price": "10"
}, {
    "name": "tiger",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "crackjack",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "monaco",
    "category": "biscuit",
    "price": "20"
}, {
    "name": "treat",
    "category": "biscuit",
    "price": "30"
}, {
    "name": "darkfantasy",
    "category": "biscuit",
    "price": "35"
}, {
    "name": "hidenseek",
    "category": "biscuit",
    "price": "50"
}, {
    "name": "bounce",
    "category": "biscuit",
    "price": "10"
}, {
    "name": "milk",
    "category": "diary",
    "price": "25"
}, {
    "name": "cheese",
    "category": "diary",
    "price": "100"
}, {
    "name": "curd",
    "category": "diary",
    "price": "25"
}, {
    "name": "butter",
    "category": "diary",
    "price": "30"
}, {
    "name": "icecream",
    "category": "diary",
    "price": "100"
}, {
    "name": "dessert",
    "category": "diary",
    "price": "100"
}, {
    "name": "custard",
    "category": "diary",
    "price": "250"
}, {
    "name": "milkshake",
    "category": "diary",
    "price": "100"
}, {
    "name": "yogurt",
    "category": "diary",
    "price": "100"
}, {
    "name": "tofu",
    "category": "diary",
    "price": "300"
}]
var searched = [];
var cart = [];
var total = 0;
var ltable = document.querySelector('#ltable');
var rtable = document.querySelector('#rtable');

function search(callback) {
    var input = document.querySelector("#grocerysearch").value;
    if (input) {
        pattern = new RegExp("[a-zA-Z]*" + input + "[a-zA-Z]*");

        data.forEach((object) => {

            if (pattern.test(object.name)) {
                searched.push(object);
                callback(object);

            }

        });
    }
}


function display(object) {
    document.querySelector("#lthead").style = "display:table-row"
    var tr = document.createElement("tr");
    tr.classList.add("remove");
    var td = document.createElement("td");
    td.classList.add("name")
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    var td3 = document.createElement("td");
    var name = document.createTextNode(object.name);
    var category = document.createTextNode(object.category);
    var price = document.createTextNode(object.price);
    var input = document.createElement("INPUT");
    td1.appendChild(category);
    td2.appendChild(price);
    input.setAttribute("type", "number");
    input.setAttribute("id", "quantity_" + object.name)
    input.classList.add("quan");
    td3.appendChild(input);
    td.appendChild(name);
    tr.appendChild(td);
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    ltable.appendChild(tr);
}

function reset() {
    document.querySelector("#lthead").style = "display:none"
    var length = document.getElementById("ltable").rows.length;
    for (i = 0; i < length - 1; i++) {
        document.querySelector(".remove").remove();

    }
    searched = [];
    document.querySelector("#grocerysearch").value = '';
}

function addtocart() {
    document.querySelector("#rthead").style = "display:table-row;"
    document.querySelector("#checkoutbutton").style = "display:block"
    var table = document.querySelector("#ltable")

    searched.forEach((element => {
        for (i = 0; i < table.rows.length - 1; i++) {
            {
                console.log(table.rows.length)
                if (document.getElementsByClassName("name")[i].innerHTML === element.name) {
                    var quantity = document.getElementById("quantity_" + element.name)
                    if (quantity.value > 0) {
                        cart.push({
                            "name": element.name,
                            "quantity": quantity.value,
                            "category": element.category,
                            "price": element.price
                        });

                    }
                }
            }
        }

    }))
}

function displayCart() {
    var rtable = document.querySelector('#rtable');
    for (i = rtable.rows.length - 1; i < cart.length; i++) {

        var tr = document.createElement("tr");
        var td = document.createElement("td");
        var td1 = document.createElement("td");
        var name = document.createTextNode(cart[i].name);
        var quantity = document.createTextNode(cart[i].quantity);
        td.appendChild(name);
        td1.appendChild(quantity);
        tr.appendChild(td);
        tr.appendChild(td1);
        tr.classList.add("rem");
        rtable.appendChild(tr);
    }


}

// 

function bill() {
    var bill = 0;
    document.querySelector("#gtotal").innerHTML='';
    cart.forEach((element => {
        bill = bill + (element.price * element.quantity)
    }))

    var h2 = document.createElement("h2");
    h2.setAttribute("id", "gtotal")
    var Total = document.createTextNode("Grand Total : " + bill);
    h2.appendChild(Total);
    document.querySelector("#rhs").appendChild(h2);

}

function autoreset() {
    if (document.querySelector("#grocerysearch").value === '') {
        reset();
    }
}

function checkout() {
    var checkout = document.querySelector('#checkout');

    document.querySelector("#checkoutbutton").style = 'display:none';
    document.querySelector("#rtable").style = 'display:none';
    document.querySelector("#checkout").style = 'display:block';
    document.querySelector("#fbill").style = 'display:block';
    document.querySelector("#cart").style = 'display:none';
    checkouttable = document.querySelector("#checkout");
    for (i = checkout.rows.length - 1; i < cart.length; i++) {
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        var td1 = document.createElement("td");
        var td2 = document.createElement("td");
        var td3 = document.createElement("td");
        var td4 = document.createElement("td");
        var name = document.createTextNode(cart[i].name);
        var category = document.createTextNode(cart[i].category);
        var price = document.createTextNode(cart[i].price);
        var quantity = document.createTextNode(cart[i].quantity);
        total_price = cart[i].price * cart[i].quantity;
        var tprice = document.createTextNode(cart[i].price * cart[i].quantity)
        td.appendChild(name)
        td1.appendChild(category);
        td2.appendChild(price);
        td3.appendChild(quantity);
        td4.appendChild(tprice);
        td.appendChild(name);
        tr.appendChild(td);
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4)
        checkouttable.appendChild(tr);
        total = total + total_price;
    
    }
    document.getElementById("billing").innerHTML = "";
    var h2 = document.createElement("h2");
    h2.setAttribute("id", "gtotal")
    var Total = document.createTextNode("Grand Total : " + total);
    h2.appendChild(Total);
    document.getElementById("billing").appendChild(h2);    
}